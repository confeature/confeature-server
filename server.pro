#-------------------------------------------------
#
# Project created by QtCreator 2014-01-15T10:47:45
#
#-------------------------------------------------

QT       += core network sql

QT       -= gui

TARGET = confeature
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += src/main.cpp \
    src/core/streamprocess.cpp \
    src/network/connectionmanager.cpp \
    src/network/streamreceiver.cpp \
    src/network/streamsender.cpp \
    src/users/streamer.cpp \
    src/users/viewer.cpp \
    src/vp8/frame.cpp \
    src/vp8/streamdecoder.cpp \
    src/vp8/streamencoder.cpp \
    src/users/user.cpp \
    src/users/conference.cpp \
    src/database/database.cpp

HEADERS += src/core/streamprocess.h \
    src/network/connectionmanager.h \
    src/network/streamreceiver.h \
    src/network/streamsender.h \
    src/users/streamer.h \
    src/users/viewer.h \
    src/vp8/frame.h \
    src/vp8/streamdecoder.h \
    src/vp8/streamencoder.h \
    src/users/user.h \
    src/users/conference.h \
    src/database/database.h
