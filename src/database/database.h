#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSettings>
#include <QDebug>
#include <QSqlError>

class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = 0);

private:
    QSqlDatabase db;
    QSettings settings;

signals:

public slots:

};

#endif // DATABASE_H
