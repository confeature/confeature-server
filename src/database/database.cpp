#include "database.h"

Database::Database(QObject *parent) : QObject(parent) {
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName(settings.value("app/mysql_hostname").toString());
    db.setDatabaseName(settings.value("app/mysql_database").toString());
    db.setUserName(settings.value("app/mysql_username").toString());
    db.setPassword(settings.value("app/mysql_password").toString());
    if(db.open()) {
        if(settings.value("app/debug_mysql").toBool()) {
            qDebug() << "[I] [Database::Database(QObject *parent)] mySQL connection established on"
                     << settings.value("app/mysql_hostname").toString();
        }
    } else {
        qCritical() << "[I] [Database::Database(QObject *parent)] Cant establish mySQL connection";
        qCritical() << "[I] [Database::Database(QObject *parent)]" << db.lastError();
    }
}
