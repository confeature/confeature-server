#include <QCoreApplication>
#include "network/connectionmanager.h"
#include "network/streamreceiver.h"
#include "network/streamsender.h"
#include "database/database.h"

int main(int argc, char *argv[])
{
    // we create our application
    QCoreApplication app(argc, argv);
    QCoreApplication::setOrganizationName("Confeature");
    QCoreApplication::setOrganizationDomain("confeatu.re");
    QCoreApplication::setApplicationName("Confeature Server");

    // then, we put some settings
    QSettings settings;

    // debugs settings
    settings.setValue("app/debug_user_memory", true);
    settings.setValue("app/debug_user_management", true);
    settings.setValue("app/debug_tcp_network", true);
    settings.setValue("app/debug_udp_network", true);
    settings.setValue("app/debug_vp8", true);
    settings.setValue("app/debug_mysql", true);

    // mysql settings
    settings.setValue("app/mysql_hostname", "localhost");
    settings.setValue("app/mysql_database", "confeature");
    settings.setValue("app/mysql_username", "confeature");
    settings.setValue("app/mysql_password", "i<3confeature");

    // first, we create a database connection
    new Database();

    // then we create the connection manager (TCP)
    new ConnectionManager();

    // finally the stream receiver and sender (UDP)
    new StreamReceiver(new StreamSender());

    // main event loop
    return app.exec();
}
