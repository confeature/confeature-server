#ifndef STREAMENCODER_H
#define STREAMENCODER_H

#include <QObject>
#include <QDebug>
#include "frame.h"

class StreamEncoder : public QObject
{
    Q_OBJECT
public:
    explicit StreamEncoder(QObject *parent = 0);

signals:
    void datagramEncoded(QByteArray datagram);

public slots:
    void encodeFrameIntoDatagram(Frame* f);

};

#endif // STREAMENCODER_H
