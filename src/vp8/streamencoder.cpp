#include "streamencoder.h"

StreamEncoder :: StreamEncoder(QObject *parent) : QObject(parent) {

}

void StreamEncoder :: encodeFrameIntoDatagram(Frame *f) {
    Q_UNUSED(f);
    QByteArray datagram;
    emit datagramEncoded(datagram);
    qCritical() << "[I] [StreamEncoder::encodeFrameIntoDatagram(Frame *f)] NOT IMPLEMENTED";
}
