#include "streamdecoder.h"

StreamDecoder :: StreamDecoder(QObject *parent) : QObject(parent) {

}

void StreamDecoder :: datagramToDecodeIntoFrame(QByteArray datagram) {
    Q_UNUSED(datagram);
    Frame* f = new Frame();
    emit frameDecoded(f);
    qCritical() << "[I] [StreamDecoder :: datagramToDecodeIntoFrame(QByteArray datagram)] : NOT IMPLEMENTED";
}
