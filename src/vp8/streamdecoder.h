#ifndef STREAMDECODER_H
#define STREAMDECODER_H

#include <QObject>
#include <QDebug>
#include "frame.h"

class StreamDecoder : public QObject
{
    Q_OBJECT
public:
    explicit StreamDecoder(QObject *parent = 0);

signals:
    void frameDecoded(Frame* f);

public slots:
    void datagramToDecodeIntoFrame(QByteArray datagram);

};

#endif // STREAMDECODER_H
