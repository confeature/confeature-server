#ifndef CONFERENCE_H
#define CONFERENCE_H

#include <QObject>
#include <QTimer>
#include <QMutex>
#include <QSettings>
#include <QtConcurrent/QtConcurrent>
#include "viewer.h"
#include "streamer.h"

class Conference : public QObject
{
    Q_OBJECT
public:
    explicit Conference(int s_ttl_in_ms = 10000, int v_ttl_in_ms = 10000, QObject *parent = 0);
    QMap<QString, Viewer*> viewers;
    QMap<QString, Streamer*> streamers;
    void addViewer(QString s, Viewer* v);
    void addStreamer(QString s, Streamer* v);
    void heartbeat(QString s);

private:
    QMutex v_mutex; // for viewer list
    QMutex s_mutex; // for streamer list
    int s_TTL;      // time to live streamers
    int v_TTL;      // time to live viewers
    QSettings settings;

signals:

public slots:
    void removeExpiredSessions();
    void removeExpiredViewers();
    void removeExpiredStreamers();

};

#endif // CONFERENCE_H
