#ifndef USER_H
#define USER_H

#include <QTcpSocket>
#include <QHostAddress>
#include <QObject>
#include <QTime>

class User : public QObject
{
    Q_OBJECT
public:
    explicit User(QObject *parent = 0);
    explicit User(QTcpSocket* socket = 0, QObject *parent = 0);
    explicit User(QHostAddress a, int p, QObject *parent = 0);
    QTime time;
    QHostAddress address;
    int port;
    QString* quality;

signals:

public slots:
    void heartBeat();

};

#endif // USER_H
