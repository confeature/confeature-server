#include "conference.h"

Conference :: Conference(int s_ttl_in_ms, int v_ttl_in_ms, QObject *parent) : QObject(parent) {
    s_TTL = s_ttl_in_ms;
    v_TTL = v_ttl_in_ms;
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(removeExpiredSessions()));
    timer->start(1000);
}

void Conference :: removeExpiredStreamers() {
    s_mutex.lock();
    QMap<QString, Streamer*>::iterator s_it = streamers.begin();
    while(s_it != streamers.end() && streamers.begin() != streamers.end()) {
        if(s_it.value()->time.elapsed() > s_TTL) {
            if(settings.value("app/debug_user_management").toBool()) {
                qDebug() << "[I] [Conference::removeExpiredStreamers()] :" << s_it.value() << "deleted";
            }
            s_it = streamers.erase(s_it);
            if(settings.value("app/debug_user_management").toBool()) {
                qDebug() << "[I] [Conference::removeExpiredStreamers()] : streamers" << streamers;
            }
        }
        ++s_it;
    }
    s_mutex.unlock();
}

void Conference :: removeExpiredViewers() {
    v_mutex.lock();
    QMap<QString, Viewer*>::iterator v_it = viewers.begin();
    while(v_it != viewers.end() && viewers.begin() != viewers.end()) {
        if(v_it.value()->time.elapsed() > v_TTL) {
            if(settings.value("app/debug_tcp_network").toBool()) {
                qDebug() << "[I] [Conference::removeExpiredViewers()] :" << v_it.value() << "deleted";
            }
            v_it = viewers.erase(v_it);
            if(settings.value("app/debug_user_memory").toBool()) {
                qDebug() << "[I] [Conference::removeExpiredViewers()] : viewers" << viewers;
            }
        }
        ++v_it;
    }
    v_mutex.unlock();
}


void Conference :: removeExpiredSessions() {
    // because there is no data dependencies between streamers & viewers,
    // we can execute theses tasks together
    QtConcurrent::run(this, &Conference::removeExpiredStreamers);
    QtConcurrent::run(this, &Conference::removeExpiredViewers);
}

void Conference :: addStreamer(QString s, Streamer *v) {
    s_mutex.lock();
    streamers[s] = v;
    s_mutex.unlock();
    if(settings.value("app/debug_user_management").toBool()) {
        qDebug() << "[I] [Conference::addStreamer(QString s, Streamer *v)] : new streamer";
    }
    if(settings.value("app/debug_user_memory").toBool()) {
        qDebug() << "[I] [Conference::addStreamer(QString s, Streamer *v)] : streamers" << streamers;
    }
}

void Conference :: addViewer(QString s, Viewer *v) {
    v_mutex.lock();
    viewers[s] = v;
    v_mutex.unlock();
    if(settings.value("app/debug_user_management").toBool()) {
        qDebug() << "[I] [Conference::addViewer(QString s, Viewer *v)] : viewer added";
    }
    if(settings.value("app/debug_user_memory").toBool()) {
        qDebug() << "[I] [Conference::addViewer(QString s, Streamer *v)] : viewers" << viewers;
    }
}

void Conference :: heartbeat(QString key) {
    Viewer* v = viewers.value(key);
    Streamer* s = streamers.value(key);
    if(v!=0) {
        v->heartBeat();
        if(settings.value("app/debug_user_management").toBool()) {
            qDebug() << "[I] [Conference::heartbeat(QString s)] : viewer heartbeat";
        }
    }
    if(s!=0) {
        s->heartBeat();
        if(settings.value("app/debug_user_management").toBool()) {
            qDebug() << "[I] [Conference::heartbeat(QString s)] : streamer heartbeat";
        }
    }
}
