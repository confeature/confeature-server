#ifndef VIEWER_H
#define VIEWER_H

#include <QTcpSocket>
#include <QHostAddress>
#include <QObject>
#include <QTime>
#include "user.h"

class Viewer : public User
{
    Q_OBJECT
public:
    explicit Viewer(QTcpSocket* socket = 0, QObject *parent = 0);

private:

signals:

public slots:

};

#endif // VIEWER_H
