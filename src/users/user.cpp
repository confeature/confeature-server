#include "user.h"

User :: User(QObject *parent) : QObject(parent) {
    time.start();
}

User :: User(QTcpSocket* socket, QObject *parent) : User(parent) {
    if(socket != 0) {
        address = socket->peerAddress();
        port = socket->peerPort();
    }
}

User :: User(QHostAddress a, int p, QObject *parent) : QObject(parent) {
    address = a;
    port = p;
}

void User :: heartBeat() {
    time.restart();
}
