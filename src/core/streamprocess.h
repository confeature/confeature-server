#ifndef STREAMPROCESS_H
#define STREAMPROCESS_H

#include <QObject>
#include <QDebug>
#include "../vp8/frame.h"

class StreamProcess : public QObject
{
    Q_OBJECT
public:
    explicit StreamProcess(QObject *parent = 0);

signals:

public slots:
    void processFrame(Frame* f);
};

#endif // STREAMPROCESS_H
