#include "connectionmanager.h"

QMap<int, Conference*> ConnectionManager::conferences;

ConnectionManager :: ConnectionManager(int port_number, QHostAddress::SpecialAddress address, QObject *parent) : QTcpServer(parent) {
    if(listen(address, port_number)) {
        QObject:: connect(this, SIGNAL(newConnection()), this, SLOT(askForConnection()));
        if(settings.value("app/debug_tcp_network").toBool()) {
            qDebug() << "[I] [ConnectionManager::ConnectionManager()] Listening to port" << port_number;
        }
    } else {
        qCritical() << "[E] [ConnectionManager::ConnectionManager()] Can't listen to port" << port_number;
        qCritical() << this->errorString();
        exit(-1);
    }
    // TODO : get conferences from SQL
    conferences[0] = new Conference();
}

void ConnectionManager :: askForConnection()
{
    emit tcpConnection();
    QTcpSocket* clientSocket = nextPendingConnection();
    if(clientSocket != 0) {
        QObject::connect(clientSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
        QObject::connect(clientSocket, SIGNAL(disconnected()), this, SLOT(closeConnection()));
        if(settings.value("app/debug_tcp_network").toBool()) {
            qDebug() << "[I] [ConnectionManager::askForConnection()] Connection received from"
                     << getAddressStringFromSocket(clientSocket);
        }
    } else {
        qCritical() << "[I] [ConnectionManager::askForConnection()] A client asked for a connection but we can't retrieve the pending connection";
    }
}

void ConnectionManager :: readMessage()
{
    QString* msg = new QString();
    QTcpSocket* clientSocket = (QTcpSocket*) sender();

    while(clientSocket->canReadLine()) {
        msg->append(clientSocket->readLine());
    }

    if(settings.value("app/debug_tcp_network").toBool()) {
        qDebug() << "[I] [ConnectionManager::readMessage()]"
                 << clientSocket->peerAddress().toString().append(":")
                    .append(QString::number(clientSocket->peerPort())).toStdString().data()
                 << msg->simplified();
    }

    parseMessage(msg);
}

void ConnectionManager :: closeConnection() {
    QTcpSocket* clientSocket = (QTcpSocket*) sender();
    if(settings.value("app/debug_tcp_network").toBool()) {
        qDebug() << "[I] [ConnectionManager::closeConnection()] End of message from"
                 << getAddressStringFromSocket(clientSocket);
    }
}

void ConnectionManager :: parseMessage(QString *msg) {
    QTcpSocket* clientSocket = (QTcpSocket*) sender();
    if(msg->contains("heartbeat")) {
        int conf_number = 0; // TODO : get that !
        Conference* c = conferences.value(conf_number);
        if(c != 0) {
            c->heartbeat(getAddressStringFromSocket(clientSocket));
        }
    } else if(msg->contains("view")) {
        int conf_number = 0; // TODO : get that !
        Conference* c = conferences.value(conf_number);
        if(c != 0) {
            Viewer* v = new Viewer(clientSocket);
            c->addViewer(getAddressStringFromSocket(clientSocket), v);
            emit viewerConnected(v);
        }
    } else if(msg->contains("stream")) {
        int conf_number = 0; // TODO : get that !
        Conference* c = conferences.value(conf_number);
        if(c!=0) {
            Streamer* s = new Streamer(clientSocket);
            conferences.value(conf_number)->addStreamer(getAddressStringFromSocket(clientSocket), s);
            emit streamerConnected(s);
        }
    } else {
        qCritical() << "[I] [ConnectionManager::parseMessage(QString *msg)] : unexpected input";
    }
}

QString ConnectionManager :: getAddressStringFromSocket(QTcpSocket* a) {
    return a->peerAddress().toString().append(":").append(QString::number(a->peerPort()));
}
