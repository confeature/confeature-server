#include "streamsender.h"

StreamSender :: StreamSender(int port, QHostAddress::SpecialAddress address, QObject *parent) : QObject(parent) {
    socket = new QUdpSocket(this);
    if(socket->bind(address, port)) {
        if(settings.value("app/debug_udp_network").toBool()) {
            qDebug() << "[I] [StreamSender::StreamSender()] Listening to port" << port;
        }
    } else {
        qCritical() << "[E] [StreamSender::StreamSender()] Can't listen to port" << port;
        qCritical() << socket->errorString();
        exit(-1);
    }

}

void StreamSender :: datagramToSend(int id_conf, QByteArray a) {
    if(settings.value("app/debug_udp_network").toBool()) {
        qDebug() << "[I] [StreamSender::StreamSender()] About to send" << a;
    }
    Conference* c = ConnectionManager::conferences.value(id_conf);
    if(c!=0) {
        foreach(Viewer* v, c->viewers) {
            socket->writeDatagram(a, v->address, v->port);
            if(settings.value("app/debug_udp_network").toBool()) {
                qDebug() << "[I] [StreamSender::StreamSender()] to" << v->address.toString() << "on port" << v->port;
            }
        }
    }
}
