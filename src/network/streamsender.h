#ifndef STREAMSENDER_H
#define STREAMSENDER_H

#include <QObject>
#include <QSettings>
#include <QUdpSocket>
#include "connectionmanager.h"
#include "../users/viewer.h"

/**
 * This class is used to send streams to viewers.
 * It owns a list of conferences, of viewers and
 * send to each viewer a packet to with stream.
 * NB : it uses a single udp socket to send streams
 * to all viewers
 */

class StreamSender : public QObject
{
    Q_OBJECT
public:
    explicit StreamSender(int port = 44402,
                          QHostAddress::SpecialAddress address = QHostAddress::AnyIPv4,
                          QObject *parent = 0);

signals:
    void frameSent();

public slots:
    void datagramToSend(int id_conf, QByteArray a);

private:
    QSettings settings;
    QUdpSocket* socket;

};

#endif // STREAMSENDER_H
