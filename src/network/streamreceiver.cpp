#include "streamreceiver.h"

StreamReceiver :: StreamReceiver(int port, QHostAddress::SpecialAddress address, QObject* parent) : QObject(parent) {
    socket = new QUdpSocket(this);
    if(socket->bind(address, port)) {
        connect(socket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
        if(settings.value("app/debug_udp_network").toBool()) {
            qDebug() << "[I] [StreamReceiver::StreamReceiver()] Listening to port" << port;
        }
    } else {
        qCritical() << "[E] [StreamReceiver::StreamReceiver()] Can't listen to port" << port;
        qCritical() << socket->errorString();
        exit(-1);
    }
}

StreamReceiver :: StreamReceiver(StreamSender *sender, int port, QHostAddress::SpecialAddress address, QObject *parent) : StreamReceiver(port, address, parent) {
    connect(this, SIGNAL(packetReceived(int, QByteArray)), sender, SLOT(datagramToSend(int, QByteArray)));
}

void StreamReceiver :: readPendingDatagrams() {
    while (socket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        socket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

        processDatagram(datagram);
    }
}

void StreamReceiver :: processDatagram(QByteArray datagram) {
    qDebug() << "[I] [StreamReceiver::StreamReceiver()] received :" << datagram;
    emit packetReceived(0, datagram);
}
