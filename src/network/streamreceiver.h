#ifndef STREAMRECEIVER_H
#define STREAMRECEIVER_H

#include <QObject>
#include <QUdpSocket>
#include <QSettings>
#include "streamsender.h"

/**
 * This class is used to receive streams from
 * streamers. It simply get it, and pass it to the decoder.
 */

class StreamReceiver : public QObject
{
    Q_OBJECT
public :
    StreamReceiver(int port = 44401,
                   QHostAddress::SpecialAddress address = QHostAddress::AnyIPv4,
                   QObject *parent = 0);

    StreamReceiver(StreamSender* sender,
                   int port = 44401,
                   QHostAddress::SpecialAddress address = QHostAddress::AnyIPv4,
                   QObject *parent = 0);

private slots :
    void readPendingDatagrams();
    void processDatagram(QByteArray datagram);

signals :
    void packetReceived(int id_conf, QByteArray datagram);

private :
    QUdpSocket *socket;
    QSettings settings;
};

#endif // STREAMRECEIVER_H
