#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QSettings>
#include "../users/viewer.h"
#include "../users/streamer.h"
#include "../users/conference.h"

class ConnectionManager : public QTcpServer
{
    Q_OBJECT
public :
    explicit ConnectionManager(int port_number = 44400,
                               QHostAddress::SpecialAddress address = QHostAddress::AnyIPv4,
                               QObject *parent = 0);

    static QMap<int, Conference*> conferences;

private slots :
    void askForConnection();
    void readMessage();
    void closeConnection();

signals :
    void tcpConnection();
    void messageReceived(QString* msg);
    void maxViewersConnected();
    void viewerConnected(Viewer* v);
    void viewerDisconnected(Viewer* v);
    void streamerConnected(Streamer* s);
    void streamerDisconnected(Streamer* s);
    void chatReceived(QString* msg);

private :
    void parseMessage(QString* msg);
    QString getAddressStringFromSocket(QTcpSocket* a);
    QSettings settings;
};

#endif // CONNECTIONMANAGER_H
